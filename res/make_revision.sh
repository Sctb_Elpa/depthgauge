#!/bin/sh -x
VERSIONFILE="res/VERSION"
t=`/usr/bin/sed 's/\./\n/g' $VERSIONFILE` 
a[0]=
c=0
for i in $t; do 
	a[c]=$i;
	((c=$c+1))
done

((a[2]=${a[2]}+1));
NEWVER=${a[0]}.${a[1]}.${a[2]}
echo Version Autoincrement: $NEWVER
echo $NEWVER > $VERSIONFILE

sed "s/VERSION/$NEWVER/" "$1" > "$2"
