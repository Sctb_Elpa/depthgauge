#ifndef TOOLTIPCONSTRUCTOR_INTERFACE_H
#define TOOLTIPCONSTRUCTOR_INTERFACE_H

#include <QString>

class toolTipConstructor_interface
{
public:
    virtual QString getTooltipText() const = 0;
};

#endif // TOOLTIPCONSTRUCTOR_INTERFACE_H
