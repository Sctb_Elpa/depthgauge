/*
 * Settings.h
 *
 *  Created on: 04.02.2013
 *      Author: tolyan
 */

#ifndef CMDLINEANALYSER_H_
#define CMDLINEANALYSER_H_

#include <QMap>
#include <QString>
#include <QVariant>

#include <settingscmdline/SimpleOpt.h>
#include <settingscmdline/settingscmdline.h>

/**
 * @brief The Settings class -  Класс-обработчик настроек программы.
 * Хранит настройки в виде пар плюч - значение
 * Возможно использовать настройки по умолчанию и чтение сохраненных в файле настройек (используется QSettings).
 * Можно добавить реакцию на ключи командной строки.
 */

class Settings: public QObject, public SettingsCmdLine::cSettingsCmdLine
{
    Q_OBJECT
public:
    /**
    * @brief Конструктор. Принимает стандартныйе параматры командной строки
    * @param argc - размер массива argv
    * @param argv - массив указателей на аргументы коммандной строки
    */
    Settings(int argc, char *argv[]);

    /**
     * @brief parse_Arg - парсер аргументов командной строки
     * @param argCode - числовой код распознанного аргумента
     * @param optText - строка - текст опции, например "-a"
     * @param ArgVal - строка, следующая ща аргументом (пустая, если не используется)
     * @param origins - предидущее состояние класса-предка cSettingsCmdLine,
     * требуется, если есть взаимное влияние опций
     * @return Заполненую структуру SettingsCmdLine::key_val_res с результом вида ключ - значение
     * <br><b>Пример:<b>
     * @code
     * struct SettingsCmdLine::key_val_res parse_Arg(int argCode,
     *            const char* optText, char* ArgVal, cSettingsCmdLine* origins)
     * {
     *  SettingsCmdLine::key_val_res result =
     *      {
     *      QString(), QVariant(), true
     *      };
     *  switch (optCode)
     *  {
     *  case OPT_A:
     *      result.key = "someoption"
     *      result.value = 10;
     *      break;
     *  case OPT_B:
     *      if (QString:num(ArgVal).toUint > 100)
     *          result.res = false;
     *      else
     *          {
     *          result.key = "value1";
     *          result.value = QString:num(ArgVal).toUint;
     *          }
     *      break;
     *  [Другие опции]
     *  default:
     *      result.res = false;
     *  }
     * return result;
     * }
     * @endcode
     */
    static struct SettingsCmdLine::key_val_res parse_Arg(int argCode,
                                                         const char* optText, char* ArgVal, cSettingsCmdLine* origins);

    /**
     * @brief SetDefaultValues - устанавливает значение по умолчанию перед началос работы
     * @param _this - указатель на класс-предок (используется аналогично this в нестатических методах)
     * <br><b>Пример:<b>
     * @code
     * void SetDefaultValues(cSettingsCmdLine* _this)
     * {
     *  _this->insert("somevaluename", sovevalue);
     *  ...
     * }
     * @endcode
     */
    static void SetDefaultValues(cSettingsCmdLine* _this);

    /**
     * @brief console_ShowUsage - функция, вызываемая при обнаружении опции с кодом 0.
     * Обычно, она соответствует вызову краткой справки.
     * @param _this - указатель на на класс-предок (используется аналогично this),
     * заполненый значениями по умолчанию
     */
    static void console_ShowUsage(cSettingsCmdLine* _this);

private:
    /**
     * @brief table - таблица указателей на функции, требуется для связывания с библиотекой libsettingscmdline
     */
    static SettingsCmdLine::pfTable table;
};

#endif /* CMDLINEANALYSER_H_ */
