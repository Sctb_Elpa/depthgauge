#ifndef SPLINECURVE_H
#define SPLINECURVE_H

#include <qwt_plot_curve.h>

#include <smoothspline/SmoothSpline.h>

class SplineCurve : public QwtPlotCurve
{
public:
    explicit SplineCurve( const QString &title = QString::null, double smoothfactor = 0);
    explicit SplineCurve( const QwtText &title, double smoothfactor = 0);
    ~SplineCurve();

    void setSamples(const QVector<QPointF> & samples);
    void setSamples( const double *xData, const double *yData, int size );
    void setSmoothFactor(double smoothfactor);

protected:
    void drawSteps ( QPainter *painter,
                     const QwtScaleMap & xMap,
                     const QwtScaleMap & yMap,
                     const QRectF & canvasRect,
                     int from, int to ) const;

    spline_range* spline_coeffs;

    double smoothfactor;
};

#endif // SPLINECURVE_H
