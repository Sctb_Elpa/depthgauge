#ifndef TEMPERATURESOLVER_H
#define TEMPERATURESOLVER_H

#include <QMap>

#include "solver.h"

class temperatureSolver : public solver
{
public:
    temperatureSolver();

    float solve(float input);
    void reset();

    bool setCoeffs(const QMap<QString, double> &coeffs);
    QMap<QString, double> getCoeffs() const;

private:
    QMap<QString, double> coeffs;
};

#endif // TEMPERATURESOLVER_H
