#ifndef PERCENTSCALEDRAW_H
#define PERCENTSCALEDRAW_H

#include <qwt_scale_draw.h>

class PercentScaleDraw : public QwtScaleDraw
{
public:
    PercentScaleDraw() {}

    virtual QwtText label(double v) const;
};

#endif // PERCENTSCALEDRAW_H
