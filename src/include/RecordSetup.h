#ifndef RECORDSETUP_H
#define RECORDSETUP_H

#include <QDialog>
#include <QVariantMap>

class QPushButton;
class QLineEdit;
class QRadioButton;
class QCheckBox;
class QSpinBox;
class QVBoxLayout;

class RecordSetup : public QDialog
{
    Q_OBJECT
public:
    explicit RecordSetup(QWidget *parent = 0, bool tiny = false);

    QVariantMap getSetup();

    QVBoxLayout *getMainlayout() const;

private:
    bool result;

    QPushButton *OkButton, *CancelButton;

    QLineEdit *FilenameEdit;
    QPushButton* SetFilenameBtn;

    QRadioButton* radiogroup_m[4];

    QSpinBox *writeInterval;
    QCheckBox *WriteLevel;
    QCheckBox *WriteLevel_pr;
    QCheckBox *WriteAtm_p;
    QCheckBox *WriteReservuar_p;
    QVBoxLayout *mainlayout;

    bool tiny;

private slots:
    void okSlot();
    void SelectFilenameSlot();
};

#endif // RECORDSETUP_H
