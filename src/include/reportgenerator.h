#ifndef REPORTGENERATOR_H
#define REPORTGENERATOR_H

#include <QDateTime>

#include "RecordSetup.h"

class QDateTimeEdit;

class ReportGenerator : public RecordSetup
{
    Q_OBJECT
public:
    explicit ReportGenerator(const QDateTime& rangeStart, const QDateTime& rangeEnd, QWidget *parent = 0);

private:
    QDateTimeEdit *from_t, *to_t;

public slots:
    void createReport(const QString& file = QString(), int devider = 0);
};

#endif // REPORTGENERATOR_H
