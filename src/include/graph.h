#ifndef GRAPH_H
#define GRAPH_H

#include <QWidget>

#include "toolTipConstructor_interface.h"
#include "historykeeper.h"
#include "splinecurve.h"

class QwtPlot;
class QxtSpanSlider;
class HistoryKeeper;
class QwtPlotCurve;
class QPushButton;

class Graph : public QWidget, public toolTipConstructor_interface
{
    Q_OBJECT
public:
    explicit Graph(QWidget *parent = 0);
    virtual ~Graph();
    QString getTooltipText() const;

private:
    QxtSpanSlider *rangeSelector;
    QwtPlot* HiResPlot, *globalPlot;

    HistoryKeeper* historyKeeper;
    QVector<SplineCurve*> *globalplotCurves;
    QVector<QwtPlotCurve*> *vertLines;
    QwtPlotCurve* HiresCurve;
    SplineCurve *buildCurve(const QString &name, const QColor &color, const int style, const int y_axis);
    QList<QPushButton*> rangeselectors;

    struct minmax
    {
        double min, max;
    };

    void updateCurve(QwtPlotCurve *curve, const QVector<QPointF> &data);

    QDateTime visableAreaStart;
    QDateTime visableAreaEnd;

    int scroll_left, scroll_right;

public Q_SLOTS:
    void btn_range_selected(bool checked);
    void update();
    void genReportRequest();
};

#endif // GRAPH_H
