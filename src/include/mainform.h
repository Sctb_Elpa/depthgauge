#ifndef MAINFORM_H
#define MAINFORM_H

#include <QMainWindow>
#include <QMutex>
#include <QDateTime>
#include <QTimer>

#define kgF2psi     (14.223)

#define _FT_VID     "0403"
#define _FT_PID     "6001"

#define DATE_WRITE_FORMAT "yyyy.MM.dd hh:mm:ss"
#define DATE_WRITE_FORMAT_FNAME_SAFE "yyyy.MM.dd_hh.mm.ss"

class QAction;
class DB00;
class QLabel;
class ModbusEngine;
class QFile;
class QTimer;
class solver;
class Graph;

class SerialDeviceEnumerator;

class Mainform : public QMainWindow
{
    Q_OBJECT
public:
    explicit Mainform(ModbusEngine* mb, QWidget *parent = 0);
    virtual ~Mainform();

public slots:
    void DetectDB00();

private:
    QLabel *PAqua, *Patm, *Gauge;
    DB00* Sensor;
    QWidget* centralWgt;
    ModbusEngine* mb;

    QAction *setupAction, *RecordAction, *OpenGraphAction;

    void Update(QList<float> values);
    void WriteToFile(QList<float> values);

    static void showDeviceInfo(QStringList List, SerialDeviceEnumerator* enumerator);

    QMutex cancelScanMutex;

    QFile* outputFile;
    QString WriteTemplate;

    char DateMode;
    unsigned int counter;
    QDateTime StartTime;

    QTimer *writetimer;

    solver *atmPresureSolver, *reservuarPresureSolver, *mmhgTommWaterSolver;

    Graph *graph;

    QTimer autoreportTimer;

private slots:
    void cancelScan();
    void generalModbusfailure();
    void recordToFile(bool startrecord);
    void miSize(bool open);
    void openSetupWindow();
    void RestartAutoreporter();
    void Wtite_autoreport();

protected:
    virtual void timerEvent(QTimerEvent* _e);
};

#endif // MAINFORM_H
