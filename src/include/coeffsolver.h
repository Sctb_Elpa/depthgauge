#ifndef COEFFSOLVER_H
#define COEFFSOLVER_H

#include "solver.h"

class coeffSolver : public solver
{
public:
    coeffSolver(double coeff = 1);

    float solve(float input);
    void reset();

    QMap<QString, double> getCoeffs() const;
    bool setCoeffs(const QMap<QString, double> &coeffs);

    static coeffSolver genLevel2percentSolver();

private:
    double coeff;
};

#endif // COEFFSOLVER_H
