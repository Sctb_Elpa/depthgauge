#ifndef SETUPWINDOW_H
#define SETUPWINDOW_H

#include <QDialog>

class QSlider;
class QDoubleSpinBox;
class QLineEdit;
class QLineEdit;
class QSpinBox;
class QCheckBox;

class SetupWindow : public QDialog
{
    Q_OBJECT
public:
    explicit SetupWindow(QWidget *parent = 0);
    
private:
    QSlider *ZeroCorrectionSlider;
    QDoubleSpinBox *ZeroCorrectionSpinbox;

    QDoubleSpinBox *PercentSpinbox, *LevelSpinbox;

    QSpinBox *AutoreportsPeriod, *AutoreportsDataCount;

    QCheckBox *Reportsenabled;

    QLineEdit* SaveDir;

    QLineEdit* resultcoeff;

    double originalZero;
    double originalP, originalLevel;

    void apply_settings();

private slots:
    void sliderValueChanged(int newVal);
    void ZeroCorrectionSpinboxChanged(double newVal);
    void coeff_changed();
    void updatePointsMax(int period);
    void BrouseCatalogs();

public slots:
    virtual void accept();
    virtual void reject();
};

#endif // SETUPWINDOW_H
