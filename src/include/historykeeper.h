#ifndef HISTORYKEEPER_H
#define HISTORYKEEPER_H

#include <QObject>
#include <QVector>
#include <QDateTime>
#include <QSqlDatabase>
#include <QPointF>

class QSqlDatabase;

class HistoryKeeper : public QObject
{
    Q_OBJECT
public:
    class dbinfo
    {
    public:
        inline dbinfo() {};
        inline dbinfo(const QDateTime &startDate, const QDateTime& lastDate, qint64 size)
        {
            this->startDate = startDate;
            this->lastDate = lastDate;
            this->size = size;
        }

        inline QDateTime StartDate() const
        {
            return startDate;
        }

        inline QDateTime LastDate() const
        {
            return lastDate;
        }

        inline qint64 dbsize() const
        {
            return size;
        }

    private:
        QDateTime startDate, lastDate;
        qint64 size;
    };

    static void instantie(const QString &filename, QObject *parent = 0);
    static HistoryKeeper* getInstance();

    dbinfo dboverview();

    int dbLen();

    QVector<QPointF> ReadRange(const QDateTime &from, const QDateTime &to, int maxpoints = -1);

    void adddata(const QDateTime& timestamp, float value);

    bool writeEnabled;

    int rangeLen(const QDateTime &from, const QDateTime &to);

protected:
    void timerEvent(QTimerEvent *e);

private:
    QSqlDatabase db;
    static HistoryKeeper* _this;

private:
    explicit HistoryKeeper(const QString &filename, QObject *parent = 0);
    ~HistoryKeeper();
};

#endif // HISTORYKEEPER_H
