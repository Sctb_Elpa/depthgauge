#ifndef PRESURESOLVER_H
#define PRESURESOLVER_H

#include "solver.h"

class PresureSolver : public solver
{
public:
    PresureSolver();

    virtual float solve(float input);

    void setT0(float T0);

    bool setCoeffs(const QMap<QString, double> &coeffs);
    QMap<QString, double> getCoeffs() const;

    virtual void reset();

private:
    QMap<QString, double> coeffs;
    float T0;
};

#endif // PRESURESOLVER_H
