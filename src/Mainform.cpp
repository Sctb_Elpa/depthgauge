#include <QVBoxLayout>
#include <QLabel>
#include <QStringList>
#include <QApplication>
#include <QSplitter>
#include <QProgressDialog>
#include <QMessageBox>
#include <QAction>
#include <QIcon>
#include <QToolBar>
#include <QVariantMap>
#include <QFile>
#include <QTimer>
#include <QMap>
#include <QTextCodec>
#include <QDir>

#include <qmodbus/ModbusEngine.h>
#include <qmodbus/common.h>
#include <settingscmdline/settingscmdline.h>
#include <mylog/mylog.h>
#include <modbus/modbus.h>

#include "reportgenerator.h"
#include "setupwindow.h"
#include "DB00.h"
#include "SerialDeviceEnumerator.h"
#include "RecordSetup.h"
#include "QDateTimeDelta.h"
#include "temperaturesolver.h"
#include "coeffsolver.h"
#include "graph.h"
#include "historykeeper.h"

#include "mainform.h"

#ifndef _DEBUG_OUTPUT
#define _DEBUG_OUTPUT   (0)
#endif

#define MMHG_TO_MMH2O   (13.595060494664)
#define MM_TO_M         (0.001 * MMHG_TO_MMH2O)

static const QString GaugeText(QObject::trUtf8("Уровень:<br><center><b><font size=\"10\">%1 <font size=\"5\">%<br>(%2 м)</h1></font></center>"));
static const QString PAquaText(QObject::trUtf8("Давление в резервуаре:<br><center><b><font size=\"6\">%1 <font size=\"4\">mmHg</font></h2></center>"));
static const QString PAtmText(QObject::trUtf8("Атмосферное давление:<br><center><b><font size=\"6\">%1 <font size=\"4\">mmHg</font></h2></center>"));
//const char qss[] = "border-style: solid; border-width: 1px; border-color: black; background-color: #e3f0ff";
const char qss[] = "border-style: solid; border-width: 1px; border-color: black; background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, \
        stop: 0 #ceeeee, stop: 0.3 #e3eeee, stop: 0.7 #e3eeee, stop: 1 #ceeeee);";

Mainform::Mainform(ModbusEngine* mb, QWidget *parent) :
    QMainWindow(parent)
{
    this->mb = mb;
    Sensor = NULL;
    outputFile = NULL;
    writetimer = NULL;
    QHBoxLayout *mainlayout = new QHBoxLayout();
    QVBoxLayout *digits = new QVBoxLayout();
    Gauge = new QLabel(GaugeText.arg("N/A").arg("N/A"));
    Gauge->setAutoFillBackground(true);
    Gauge->setTextFormat(Qt::RichText);
    Gauge->setStyleSheet(qss);
    PAqua = new QLabel(PAquaText.arg("N/A"));
    PAqua->setAutoFillBackground(true);
    PAqua->setTextFormat(Qt::RichText);
    PAqua->setStyleSheet(qss);
    Patm = new QLabel(PAtmText.arg("N/A"));
    Patm->setAutoFillBackground(true);
    Patm->setTextFormat(Qt::RichText);
    Patm->setStyleSheet(qss);
    digits->addWidget(Gauge);
    //digits->addWidget(new QSplitter(Qt::Horizontal));
    digits->addWidget(PAqua);
    //digits->addWidget(new QSplitter(Qt::Horizontal));
    digits->addWidget(Patm);
    digits->addStretch();

    QSizePolicy left(QSizePolicy::Preferred, QSizePolicy::Preferred);
    left.setHorizontalStretch(0);
    Gauge->setSizePolicy(left);
    PAqua->setSizePolicy(left);
    Patm->setSizePolicy(left);

    centralWgt = new QWidget;

    setupAction = new QAction(trUtf8("Настройки"), this);
    setupAction->setIcon(QIcon(":/res/Tools1.png"));

    RecordAction = new QAction(trUtf8("Запись в файл"), this);
    RecordAction->setIcon(QIcon(":/res/media_record.png"));
    RecordAction->setCheckable(true);

    OpenGraphAction = new QAction(trUtf8("Графики"), this);
    OpenGraphAction->setIcon(QIcon(":/res/graph.png"));
    OpenGraphAction->setCheckable(true);
    OpenGraphAction->setChecked(SettingsCmdLine::settings->value("Interface/Graph", false).toBool());

    QToolBar *toolbar = new QToolBar;
    toolbar->addAction(setupAction);
    toolbar->addSeparator();
    toolbar->addAction(RecordAction);
    toolbar->addSeparator();
    toolbar->addAction(OpenGraphAction);
    addToolBar(Qt::TopToolBarArea, toolbar);

    mainlayout->addLayout(digits);

    HistoryKeeper::instantie(SettingsCmdLine::settings->value("Global/Database_name").toString());
    HistoryKeeper::getInstance()->startTimer(SettingsCmdLine::settings->value("Global/DB_write_period").toInt() * 1001);

    graph = new Graph(this);
    QSizePolicy right(QSizePolicy::Preferred, QSizePolicy::Preferred);
    left.setHorizontalStretch(1);
    graph->setSizePolicy(right);
    mainlayout->addWidget(graph);

    graph->setVisible(OpenGraphAction->isChecked());
    if (OpenGraphAction->isChecked())
        resize(800, 600);

    centralWgt->setLayout(mainlayout);
    setCentralWidget(centralWgt);

    mmhgTommWaterSolver = new coeffSolver(MM_TO_M);

    QMap<QString, double> coeffs_Atm, coeffs_Res;
    foreach (QString key, SettingsCmdLine::settings->keys()) {
        if (key.startsWith("AtmCoefficients/"))
        {
            coeffs_Atm.insert(key.remove("AtmCoefficients/"), SettingsCmdLine::settings->value(key).toDouble());
            continue;
        }
        if (key.startsWith("WaterCoefficients/"))
        {
            coeffs_Res.insert(key.remove("WaterCoefficients/"), SettingsCmdLine::settings->value(key).toDouble());
        }
    }

    atmPresureSolver = new temperatureSolver;
    atmPresureSolver->setCoeffs(coeffs_Atm);
    reservuarPresureSolver = new temperatureSolver;
    reservuarPresureSolver->setCoeffs(coeffs_Res);

    connect(mb, SIGNAL(ModbusGeneralFailure(int)), this, SLOT(generalModbusfailure()));
    connect(RecordAction, SIGNAL(triggered(bool)), this, SLOT(recordToFile(bool)));
    connect(OpenGraphAction, SIGNAL(triggered(bool)), graph, SLOT(setVisible(bool)));
    connect(OpenGraphAction, SIGNAL(triggered(bool)), this, SLOT(miSize(bool)));
    connect(setupAction, SIGNAL(triggered()), this, SLOT(openSetupWindow()));

    connect(&autoreportTimer, SIGNAL(timeout()), this, SLOT(Wtite_autoreport()));
    RestartAutoreporter();
}

Mainform::~Mainform()
{
    (*SettingsCmdLine::settings)["Interface/Graph"] = OpenGraphAction->isChecked();
    if (RecordAction->isChecked())
        RecordAction->setChecked(false);

    delete atmPresureSolver;
    delete reservuarPresureSolver;
    delete mmhgTommWaterSolver;
}



void Mainform::DetectDB00()
{
    SerialDeviceEnumerator* enumerator = SerialDeviceEnumerator::instance();
    QStringList avalable = enumerator->devicesAvailable();
#if (_DEBUG_OUTPUT == 1)
    showDeviceInfo(avalable, enumerator);
#endif
    foreach(QString portname, avalable)
    {
        enumerator->setDeviceName(portname);
        if ((enumerator->vendorID() != _FT_VID) || (enumerator->productID() != _FT_PID) || (enumerator->isBusy() == true))
            avalable.removeOne(portname);
        else
            if (portname.at(0) != '/')
            {
                QString number = portname;
                number.remove(0, 3);
                if (number.toInt() > 9)
                {
                    avalable.removeOne(portname);
                    avalable.append("\\\\.\\" + portname);
                }
            }
    }
    enumerator->deleteLater();

    if (avalable.empty())
    {
        QMessageBox::critical(this, trUtf8("Поддерживаемых устройств не найдено"),
                              trUtf8("Не найдено виртуальных последовательных портов FTDI.\nУбедитесь, что устройство подключено."));
        qApp->exit(2);
    }

    LOG_INFO(trUtf8("Start Scaning ports: %1").arg(avalable.join(", ")));
    QProgressDialog *progress = new QProgressDialog(this);
    connect(progress, SIGNAL(canceled()), this, SLOT(cancelScan()));
    progress->setModal(true);
    progress->setMinimum(1);
    progress->setMaximum(255);
    progress->setValue(1);
    progress->show();

    cancelScanMutex.lock();
    DB00* tempSensor = NULL;
    unsigned char addres;
    uint32_t tv_sec, tv_usec;
    modbus_get_response_timeout(mb->getModbusContext(), &tv_sec, &tv_usec);

    foreach(QString portname, avalable)
    {
        (*SettingsCmdLine::settings)["Global/Port"] = portname;
        mb->RestartConnection();
        modbus_set_response_timeout(mb->getModbusContext(), 0, 40000);
        addres = 1;
        while (addres < 255)
        {
            if (cancelScanMutex.tryLock())
            {
                cancelScanMutex.unlock();
                break; // cancel?
            }
            qApp->processEvents();
            progress->setLabelText(trUtf8("Сканирование порта %1, адрес 0x%2").arg(portname).arg(addres, 0, 16));
            progress->setValue(addres);
            tempSensor = new DB00(mb, addres++);
            if (tempSensor->test())
            {
                LOG_INFO(trUtf8("Sensor DB00 found at port %1, adress 0x%2").arg(portname).arg(addres, 0, 16));
                cancelScanMutex.unlock();
                break; //found!
            }
            else
                __DELETE(tempSensor);
        }
        if (cancelScanMutex.tryLock())
        {
            break; // cancel || found
        }
    }

    progress->deleteLater();

    if (tempSensor == NULL)
    {
        QMessageBox::critical(this, trUtf8("Датчиков не обнаружено"),
                              trUtf8("Сканирование завершено - поддерживаемых устройств не найдено."));
        //qApp->exit(1);
    }
    else
    {
        modbus_set_response_timeout(mb->getModbusContext(), tv_sec, tv_usec);
        Sensor = tempSensor;
    }
}

void Mainform::Update(QList<float> values)
{
    if (values.size() != 2)
        return;

    float Atm = atmPresureSolver->solve(values.at(1));
    float Aqua = reservuarPresureSolver->solve(values.at(0));
    float level = mmhgTommWaterSolver->solve(Aqua - Atm);
    level += SettingsCmdLine::settings->value("Global/ZeroOffset").toDouble();
    float level_pr = coeffSolver::genLevel2percentSolver().solve(level);

    PAqua->setText(PAquaText.arg(Aqua, 0, 'f', 2));
    Patm->setText(PAtmText.arg(Atm, 0, 'f', 2));
    Gauge->setText(GaugeText.arg(level_pr, 0, 'f', 1).arg(level, 0, 'f', 2));

    HistoryKeeper::getInstance()->adddata(QDateTime::currentDateTime(), level);
}

void Mainform::WriteToFile(QList<float> values)
{
    if (outputFile != NULL)
    {
        if (writetimer && writetimer->isActive())
            return;
        if (writetimer)
            writetimer->start();
        QString Firstcolumn;
        switch (DateMode)
        {
        case 0:
            Firstcolumn = QDateTime::currentDateTime().toString(DATE_WRITE_FORMAT) + ";";
            break;
        case 1:
            Firstcolumn = QDateTimeDelta(QDateTime::currentDateTime(), StartTime) + ";";
            break;
        case 2:
            Firstcolumn = QString::number(counter++) + ";";
            break;
        case 3:
            Firstcolumn = "";
            break;
        }
        QString Measures = WriteTemplate;

        float Atm = atmPresureSolver->solve(values.at(1));
        float Aqua = reservuarPresureSolver->solve(values.at(0));
        float level = mmhgTommWaterSolver->solve(Aqua - Atm);

        level += SettingsCmdLine::settings->value("Global/ZeroOffset").toDouble();
        float level_pr = coeffSolver::genLevel2percentSolver().solve(level);

        Measures.replace("%L%", QString::number(
                             level, 'f', 2));
        Measures.replace("%LP%", QString::number(
                             level_pr, 'f', 1));
        Measures.replace("%A%", QString::number(
                             Atm, 'f', 2));
        Measures.replace("%R%", QString::number(
                             Aqua, 'f', 2));
#if (_DEBUG_OUTPUT == 1)
        LOG_DEBUG(Firstcolumn + Measures);
#endif
        QByteArray writestring;
        writestring.append(Firstcolumn);
        writestring.append(Measures);
        outputFile->write(writestring);
    }
}

void Mainform::showDeviceInfo(QStringList List, SerialDeviceEnumerator *enumerator)
{
    foreach(QString str, List)
    {
        enumerator->setDeviceName(str);
        LOG_INFO(trUtf8(">>> info about: %1").arg(enumerator->name()));

        LOG_INFO(trUtf8("-> description  : %1").arg(enumerator->description()));
        LOG_INFO(trUtf8("-> driver       : %1").arg(enumerator->driver()));
        LOG_INFO(trUtf8("-> friendlyName : %1").arg(enumerator->friendlyName()));
        LOG_INFO(trUtf8("-> hardwareID   : %1").arg(enumerator->hardwareID().join(" ")));
        LOG_INFO(trUtf8("-> locationInfo : %1").arg(enumerator->locationInfo()));
        LOG_INFO(trUtf8("-> manufacturer : %1").arg(enumerator->manufacturer()));
        LOG_INFO(trUtf8("-> productID    : %1").arg(enumerator->productID()));
        LOG_INFO(trUtf8("-> service      : %1").arg(enumerator->service()));
        LOG_INFO(trUtf8("-> shortName    : %1").arg(enumerator->shortName()));
        LOG_INFO(trUtf8("-> subSystem    : %1").arg(enumerator->subSystem()));
        LOG_INFO(trUtf8("-> systemPath   : %1").arg(enumerator->systemPath()));
        LOG_INFO(trUtf8("-> vendorID     : %1").arg(enumerator->vendorID()));

        LOG_INFO(trUtf8("-> revision     : %1").arg(enumerator->revision()));
        LOG_INFO(trUtf8("-> bus          : %1").arg(enumerator->bus()));
        //
        LOG_INFO(trUtf8("-> is exists    : %1").arg(enumerator->isExists()));
        LOG_INFO(trUtf8("-> is busy      : %1").arg(enumerator->isBusy()));
    }
}

void Mainform::cancelScan()
{
    cancelScanMutex.unlock();
}

void Mainform::generalModbusfailure()
{
    recordToFile(false);
    QMessageBox::critical(this, trUtf8("Соединение разорвано."), trUtf8("Соединение разорвано, кабель был отсоединен?"));
    qApp->exit(3);
}

void Mainform::recordToFile(bool startrecord)
{
    if (startrecord)
    {
        RecordSetup setupwindow;
        setupwindow.exec();
        QVariantMap setup = setupwindow.getSetup();
        if (setup.isEmpty())
        {
            RecordAction->setChecked(false);
            return;
        }
#if (_DEBUG_OUTPUT == 1)
        foreach(QString key, setup.keys())
        {
            LOG_INFO(trUtf8("%1 -> %2").arg(key).arg(setup.value(key, "").toString()));
        }
#endif
        outputFile = new QFile(setup.value("filename").toString());
        if (!outputFile->open(QIODevice::WriteOnly | QIODevice::Text))
        {
            LOG_ERROR(trUtf8("Failed to open file %1.").arg(setup.value("filename").toString()));
            __DELETE(outputFile);
            RecordAction->setChecked(false);
        }
        else
        {
            DateMode = (char) setup.value("dateMode").toInt();
            (*SettingsCmdLine::settings)["SaveSettings/DateMode"] = setup.value("dateMode");
            QString firstcolumname;
            switch (DateMode)
            {
            case 0:
                firstcolumname = trUtf8("Дата;");
                break;
            case 1:
                firstcolumname = trUtf8("Таймер;");
                StartTime = QDateTime::currentDateTime();
                break;
            case 2:
                firstcolumname = trUtf8("№ п/п;");
                counter = 0;
                break;
            }

            (*SettingsCmdLine::settings)["SaveSettings/WL"] = setup.value("WL");
            (*SettingsCmdLine::settings)["SaveSettings/WA"] = setup.value("WA");
            (*SettingsCmdLine::settings)["SaveSettings/WR"] = setup.value("WR");

            QString headText = trUtf8("%1%2%3%4").arg(
                        setup.value("WL").toBool() ? trUtf8("Уровень [м];") : ("")).arg(
                        setup.value("WLP").toBool() ? trUtf8("Уровень [%];") : ("")).arg(
                        setup.value("WA").toBool() ? trUtf8("Атмосферное давление [мм Hg];") : ("")).arg(
                        setup.value("WR").toBool() ? trUtf8("Давление в резервуаре [мм Hg];") : (""));
            headText[headText.length() - 1] = QChar('\n');
            QTextCodec *codec = QTextCodec::codecForName("Windows-1251");
            outputFile->write(codec->fromUnicode(firstcolumname + headText));

            WriteTemplate = trUtf8("%1%2%3%4").arg(
                        setup.value("WL").toBool() ? ("%L%;") : ("")).arg(
                        setup.value("WLP").toBool() ? ("%LP%;") : ("")).arg(
                        setup.value("WA").toBool() ? ("%A%;") : ("")).arg(
                        setup.value("WR").toBool() ? ("%R%;") : (""));
            *(WriteTemplate.end() - 1) = '\n';
            RecordAction->setIcon(QIcon(":/res/media_stop.png"));
            LOG_INFO(trUtf8("Start recording to file %1").arg(setup.value("filename").toString()));
            (*SettingsCmdLine::settings)["SaveSettings/filename"] = setup.value("filename");
            writetimer = new QTimer;
            uint interval = setup.value("WI", 1).toUInt();
            (*SettingsCmdLine::settings)["SaveSettings/Interval"] = interval;
            writetimer->setInterval(interval * 1000);
            writetimer->setSingleShot(true);
        }
    }
    else
    {
        if (writetimer != NULL)
        {
            __DELETE(writetimer);
            writetimer = NULL;
        }
        if (outputFile != NULL)
        {
            outputFile->close();
            __DELETE(outputFile);
        }
        LOG_INFO(trUtf8("Record stoped"));
        RecordAction->setIcon(QIcon(":/res/media_record.png"));
    }
}

void Mainform::miSize(bool open)
{
    if (!open)
    {
        centralWgt->resize(centralWgt->minimumSizeHint());
        this->adjustSize();
    }
    else
    {
        resize(800, 600);
    }
}

void Mainform::openSetupWindow()
{
    SetupWindow *w = new SetupWindow(this);
    w->setAttribute(Qt::WA_DeleteOnClose);
    if (w->exec() == QDialog::Accepted) {
        RestartAutoreporter();
    }
}

void Mainform::RestartAutoreporter()
{
    autoreportTimer.stop();
    if (SettingsCmdLine::settings->value("AutoReport/Enabled").toBool()) {
        QDir dir(SettingsCmdLine::settings->value("AutoReport/Directory").toString());
        if (!dir.exists())
            if (!dir.mkpath(".")) { //TODO
                LOG_ERROR(trUtf8("Failed to create directory %1").arg(dir.path()));
                return;
            }
        autoreportTimer.setInterval(
                    SettingsCmdLine::settings->value("AutoReport/Period").toInt() * 1000);
        autoreportTimer.start();
    }
}

void Mainform::Wtite_autoreport()
{
    QDateTime now = QDateTime::currentDateTime();
    QDateTime start(
                QDateTime::fromMSecsSinceEpoch(now.toMSecsSinceEpoch() -
                                               SettingsCmdLine::settings->value(
                                                   "AutoReport/Period").toInt() * 1000));
    QString fname(SettingsCmdLine::settings->value("AutoReport/Directory").toString() +
                  "/" + start.toString(DATE_WRITE_FORMAT_FNAME_SAFE) + ".csv");

    ReportGenerator rg(start, now, this);
    int devider = start.secsTo(now) /
        SettingsCmdLine::settings->value("AutoReport/Points").toInt();
    rg.createReport(fname, devider);
}

void Mainform::timerEvent(QTimerEvent *_e)
{
    if (Sensor == NULL)
        return;
    bool res;
    QList<float> measuredValues = Sensor->getFreqs(&res);
    if ((!res) || measuredValues.size() != 2)
        return;
    Update(measuredValues);
    WriteToFile(measuredValues);

    if (graph->isVisible())
        graph->update();
}
