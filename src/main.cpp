//-----------------------------------------------------------------
#include <QApplication>
#include <QThread>
#include <QAction>
#include <QString>
#include <QTimer>
#include <QIcon>
#include <stdexcept>
//-----------------------------------------------------------------
#include <qmodbus/common.h>
#include <qmodbus/Sleeper.h>
#include <qmodbus/ModbusEngine.h>
#include <qmodbus/ModbusRequest.h>
#include <mylog/mylog.h>
//-----------------------------------------------------------------
#include "Settings.h"
#include "mainform.h"
//-----------------------------------------------------------------

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    app.setApplicationName(QObject::trUtf8("Дитчик уровня"));
    app.setWindowIcon(QIcon(":/res/gauge.png"));

    //настройко-держатель
    Settings _settings(argc, argv);

    //Лог
    MyLog::myLog _LOG;
#ifndef NDEBUG
    _LOG.setLogLevel(MyLog::LOG_DEBUG);
#endif

    // синхронно-асинхронный движок Modbus
    ModbusEngine _ModbusCore;

    Mainform* mainform; // Главная форма

    try
    {
        mainform = new Mainform(&_ModbusCore);
    }
    catch (std::runtime_error e)
    {
        LOG_FATAL(QObject::trUtf8("Fatal error: %1").arg(e.what()));
        return -1;
    }

    mainform->show();

    QTimer::singleShot(100, mainform, SLOT(DetectDB00()));
    mainform->startTimer(500);

    int res = app.exec();

    delete mainform;

    return res;
}
//-----------------------------------------------------------------
