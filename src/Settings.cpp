/*
 * Settings.cpp
 *
 *  Created on: 04.02.2013
 *      Author: tolyan
 */

#include <cstdio>
#include <iostream>

#include <QIODevice>

#include <mylog/mylog.h>

#include "Settings.h"

#ifdef WIN32
#define _DEFAULT_PORT	"COM1"
#else
#define _DEFAULT_PORT	"/dev/ttyUSB0"
#endif
#define _DEFAULT_SPEED	(38400)

/// Типы ключей
enum enArgs
{
    OPT_HELP,
};

/// Таблица ключей программы
static CSimpleOpt::SOption g_rgOptions[] =
{
    {
        OPT_HELP, _T("-h"), SO_NONE
    }, /// "-h"
    {
        OPT_HELP, _T("--help"), SO_NONE
    }, /// "--help"
    SO_END_OF_OPTIONS
    // END
};

/// Таблица указателей
SettingsCmdLine::pfTable Settings::table =
{
    g_rgOptions,
    Settings::SetDefaultValues,
    Settings::console_ShowUsage,
    Settings::parse_Arg
};

void Settings::console_ShowUsage(cSettingsCmdLine* _this)
{
    std::cout
            << trUtf8(
                   "Usage: DepthGauge [-h | --help]\n\
                   \n").toLatin1().data();
}

struct SettingsCmdLine::key_val_res Settings::parse_Arg(int optCode,
          const char* optText, char *ArgVal, cSettingsCmdLine* origins)
{
    SettingsCmdLine::key_val_res result =
    {
            QString(), QVariant(), true
    };
    switch (optCode)
    {
    default:
        result.res = false;
        break;
    }
    return result;
}

void Settings::SetDefaultValues(cSettingsCmdLine* _this)
{
    _this->insert("Global/Port", _DEFAULT_PORT);
    _this->insert("Global/Baud", 57600);
    _this->insert("Log/Level", MyLog::LOG_INFO);
    _this->insert("Log/NoFile", true);
    _this->insert("Global/Database_name", "res/history.sqlite");
    _this->insert("Global/DB_write_period", 1);
    _this->insert("Global/ZeroOffset", 0.0);

    _this->insert("AtmCoefficients/C1", 1);
    _this->insert("AtmCoefficients/C2", 0);
    _this->insert("AtmCoefficients/C3", 0);
    _this->insert("AtmCoefficients/F0", 0);
    _this->insert("AtmCoefficients/P0", 0);

    _this->insert("WaterCoefficients/C1", 1);
    _this->insert("WaterCoefficients/C2", 0);
    _this->insert("WaterCoefficients/C3", 0);
    _this->insert("WaterCoefficients/F0", 0);
    _this->insert("WaterCoefficients/P0", 0);

    _this->insert("Graph/LeftScrollPos", 0);
    _this->insert("Graph/RightScrollPos", 100);
    _this->insert("Graph/Percent", 100.0);
    _this->insert("Graph/Level", 5.5);

    _this->insert("AutoReport/Directory", "reports");
    _this->insert("AutoReport/Period", 2 * 60 * 60);
    _this->insert("AutoReport/Points", 10);
    _this->insert("AutoReport/Enabled", true);
}

Settings::Settings(int argc, char *argv[]) :
    QObject(NULL), cSettingsCmdLine(argc, argv, "res/Settings.ini", &table)
  /// параметр 3 устанавливает имя файла настроек программы
{
}
