#include <stdexcept>

#include <QVBoxLayout>
#include <QFont>
#include <QAction>
#include <QGroupBox>
#include <QPushButton>
#include <QList>
#include <QVector>

#include <mylog/mylog.h>

#include <qwt_plot.h>
#include <qwt_scale_widget.h>
#include <qwt_symbol.h>

#include "percentscaledraw.h"
#include "coeffsolver.h"
#include "qxtspanslider.h"
#include "stocktimescaledraw.h"
#include "graph.h"
#include "historykeeper.h"
#include "reportgenerator.h"

#define SCROLL_SIZE         500
#define MIN_DIFF            10
#define OVERVIEW_POINTS     500
#define ZAPAS               0.1

#define Y_MIN               -1
#define Y_MAX               100

#define historyKeeper       HistoryKeeper::getInstance()

static QDateTime QDate_sub_time(const QDateTime& src, int hours)
{
    {
        QTime s = src.time().addSecs(-hours * 60 * 60);
        if (s >= QTime(12, 0, 0)) {
            return QDateTime(src.date().addDays(-1), s);
        } else {
            return QDateTime(src.date(), s);
        }
    }
}

Graph::Graph(QWidget *parent) :
    QWidget(parent)
{
    scroll_left = SettingsCmdLine::settings->value("Graph/LeftScrollPos").toInt();
    scroll_right = SettingsCmdLine::settings->value("Graph/RightScrollPos").toInt();
    if (scroll_left < 0 || scroll_left > (SCROLL_SIZE - 1))
        scroll_left = 0;
    if (scroll_right < 1 || scroll_right > SCROLL_SIZE)
        scroll_right = SCROLL_SIZE;

    QVBoxLayout *mainlayout = new QVBoxLayout;
    HiResPlot = new QwtPlot;
    QSizePolicy VertGrowPolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
    VertGrowPolicy.setVerticalStretch(10);
    HiResPlot->setSizePolicy(VertGrowPolicy);
    HiResPlot->setAxisScaleDraw(QwtPlot::xBottom, new StockTimeScaleDraw(trUtf8("dd.MM\nhh.mm")));
    HiResPlot->setAxisScaleDraw(QwtPlot::yRight, new PercentScaleDraw());
    HiResPlot->setAxisLabelRotation(QwtPlot::xBottom, -60);
    HiResPlot->enableAxis(QwtPlot::yLeft, false);
    HiResPlot->enableAxis(QwtPlot::yRight, true);
    QFont f = HiResPlot->axisFont(QwtPlot::yRight);
    f.setPointSize(7);
    HiResPlot->setAxisFont(QwtPlot::yRight, f);
    HiResPlot->setAxisFont(QwtPlot::xBottom, f);
    mainlayout->addWidget(HiResPlot);
    HiResPlot->setAxisScale(QwtPlot::yRight, Y_MIN, Y_MAX); // in %

    globalPlot = new QwtPlot;
    globalPlot->setAxisScale(QwtPlot::yLeft, Y_MIN, Y_MAX); // in %
    QSizePolicy noVertGrowPolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
    noVertGrowPolicy.setVerticalStretch(1);
    globalPlot->setSizePolicy(noVertGrowPolicy);
    globalPlot->axisScaleDraw(QwtPlot::xBottom)->enableComponent(QwtAbstractScaleDraw::Labels, false);
    globalPlot->axisScaleDraw(QwtPlot::xBottom)->enableComponent(QwtAbstractScaleDraw::Ticks, false);
    globalPlot->axisScaleDraw(QwtPlot::yLeft)->enableComponent(QwtAbstractScaleDraw::Labels, false);
    globalPlot->axisScaleDraw(QwtPlot::yLeft)->enableComponent(QwtAbstractScaleDraw::Ticks, false);
    globalPlot->axisScaleDraw(QwtPlot::yLeft)->enableComponent(QwtAbstractScaleDraw::Backbone, false);
    globalPlot->setMinimumHeight(50);
    mainlayout->addWidget(globalPlot);

    QVBoxLayout *vbox = new QVBoxLayout;
    QHBoxLayout *btnslayout = new QHBoxLayout;

    btnslayout->addStretch();

    const char *btns_names[] = {"Месяц", "Неделя", "День", "12 часов", "2 часа"};

    for (int i = 0; i < sizeof(btns_names) / sizeof(char*); ++i) {
        QPushButton *b;
        b = new QPushButton(trUtf8(btns_names[i]));
        b->setCheckable(true);
        rangeselectors.append(b);
        connect(b, SIGNAL(clicked(bool)), this, SLOT(btn_range_selected(bool)));
        btnslayout->addWidget(b);
    }

    vbox->addLayout(btnslayout);

    rangeSelector = new QxtSpanSlider();
    rangeSelector->setOrientation(Qt::Horizontal);
    rangeSelector->setMinimum(0);
    rangeSelector->setMaximum(SCROLL_SIZE);
    rangeSelector->setHandleMovementMode(QxtSpanSlider::NoOverlapping);
    rangeSelector->setLowerValue(scroll_left);
    rangeSelector->setUpperValue(scroll_right);
    rangeSelector->setToolTipconstructor(this);
    vbox->addWidget(rangeSelector);

    QGroupBox *rangeSelbox = new QGroupBox(trUtf8("Выбор интервала"));
    rangeSelbox->setLayout(vbox);

    mainlayout->addWidget(rangeSelbox);

    setLayout(mainlayout);

    globalplotCurves = new QVector<SplineCurve*>;
    globalplotCurves->append(buildCurve("before", QColor(Qt::blue), QwtSymbol::NoSymbol, QwtPlot::yLeft));
    globalplotCurves->at(0)->setBrush(QBrush(QColor(0, 0, 255, 40)));
    globalplotCurves->append(buildCurve("current", QColor(Qt::magenta), QwtSymbol::NoSymbol, QwtPlot::yLeft));
    globalplotCurves->at(1)->setBrush(QBrush(QColor(255, 0, 0, 40)));
    globalplotCurves->append(buildCurve("after", QColor(Qt::blue), QwtSymbol::NoSymbol, QwtPlot::yLeft));
    globalplotCurves->at(2)->setBrush(QBrush(QColor(0, 0, 255, 40)));
    vertLines = new QVector<QwtPlotCurve*>;
    for (int i = 0; i < 2; ++i)
    {
        QPen pen = QPen(Qt::black);
        pen.setWidth(1);
        QwtPlotCurve *curve = new QwtPlotCurve;
        curve->setPen(pen);
        curve->setRenderHint(QwtPlotItem::RenderAntialiased);
        curve->setAxes(QwtPlot::xBottom, QwtPlot::yLeft);
        curve->setStyle(QwtPlotCurve::Lines);
        vertLines->append(curve);
        curve->attach(globalPlot);
    }

    foreach (QwtPlotCurve* curve, *globalplotCurves)
        curve->attach(globalPlot);

    HiresCurve = buildCurve("HiresCurve", Qt::blue, QwtSymbol::NoSymbol, QwtPlot::yRight);
    HiresCurve->setBrush(QBrush(QColor(0, 0, 255, 40)));
    HiresCurve->attach(HiResPlot);

    QAction * AgenReport = new QAction(QIcon(":/res/task_report_hot.png"), trUtf8("Создать отчет"), this);
    globalPlot->setContextMenuPolicy(Qt::ActionsContextMenu);
    globalPlot->addAction(AgenReport);
    HiResPlot->setContextMenuPolicy(Qt::ActionsContextMenu);
    HiResPlot->addAction(AgenReport);

    connect(AgenReport, SIGNAL(triggered()), this, SLOT(genReportRequest()));

    update();
}

Graph::~Graph()
{
    (*SettingsCmdLine::settings)["Graph/LeftScrollPos"] = scroll_left;
    (*SettingsCmdLine::settings)["Graph/RightScrollPos"] = scroll_right;
}

QString Graph::getTooltipText() const
{
    return trUtf8("%1 --> %2").arg(visableAreaStart.toString()).arg(visableAreaEnd.toString());
}

static QDateTime calctimeLimit(const HistoryKeeper::dbinfo & dbinfo, int pos) {
    QDateTime amonthBack = dbinfo.LastDate().addMonths(-1);
    qint64 start = amonthBack.toMSecsSinceEpoch();
    qint64 end = dbinfo.LastDate().toMSecsSinceEpoch();
    return QDateTime::fromMSecsSinceEpoch(start + (end - start) / SCROLL_SIZE * pos);
}

void Graph::update()
{
    HistoryKeeper::dbinfo dbinfo;
    try
    {
        dbinfo = historyKeeper->dboverview();
    }
    catch(std::exception e)
    {
        return;
    }

    if (dbinfo.dbsize() == 0)
        return;

    if (rangeSelector->isEnabled()) {
        visableAreaStart = calctimeLimit(dbinfo, rangeSelector->lowerValue());
        visableAreaEnd = calctimeLimit(dbinfo, rangeSelector->upperValue());
    } else {
        btn_range_selected(true);
    }

    // last mounth
    QDateTime oneMonthback = dbinfo.LastDate().addMonths(-1);

    QVector<QPointF> visableArea;
    try
    {
        updateCurve(globalplotCurves->at(0),
                    historyKeeper->ReadRange(oneMonthback, visableAreaStart, OVERVIEW_POINTS));
        visableArea = historyKeeper->ReadRange(visableAreaStart, visableAreaEnd, OVERVIEW_POINTS);
        updateCurve(globalplotCurves->at(1), visableArea);
        updateCurve(globalplotCurves->at(2),
                    historyKeeper->ReadRange(visableAreaEnd, dbinfo.LastDate(), OVERVIEW_POINTS));
    }
    catch(std::exception e)
    {
        return;
    }

    QVector<QPointF> vertLine;
    vertLine.append(QPointF(visableAreaStart.toMSecsSinceEpoch(), Y_MIN));
    vertLine.append(QPointF(visableAreaStart.toMSecsSinceEpoch(), Y_MAX));
    vertLines->at(0)->setSamples(vertLine);
    vertLine.clear();
    vertLine.append(QPointF(visableAreaEnd.toMSecsSinceEpoch(), Y_MIN));
    vertLine.append(QPointF(visableAreaEnd.toMSecsSinceEpoch(), Y_MAX));
    vertLines->at(1)->setSamples(vertLine);

    globalPlot->setAxisScale(QwtPlot::xBottom,
                             oneMonthback.toMSecsSinceEpoch(), dbinfo.LastDate().toMSecsSinceEpoch());
    //globalPlot->setAxisScale(QwtPlot::yRight, yRange.min - range * ZAPAS, yRange.max + range * ZAPAS);
    globalPlot->replot();

    //---

    if (!visableArea.isEmpty())
    {
        HiResPlot->setAxisScale(QwtPlot::xBottom,
                                visableArea.first().x(), visableArea.last().x());
    }
    updateCurve(HiresCurve, visableArea);
    HiResPlot->replot();

    //QWidget::update();
}

void Graph::genReportRequest()
{
    ReportGenerator *gen = new ReportGenerator(visableAreaStart, visableAreaEnd);
    gen->setAttribute(Qt::WA_DeleteOnClose, true);
    gen->exec();
}

SplineCurve *Graph::buildCurve(const QString &name, const QColor &color, const int style, const int y_axis)
{
    QPen pen = QPen(color);
    pen.setWidth(2);
    QwtSymbol* symbol = new QwtSymbol;
    symbol->setStyle((QwtSymbol::Style)style);
    symbol->setSize(5);
    symbol->setPen(pen);
    SplineCurve* curve = new SplineCurve(name);
    curve->setSymbol(symbol);
    curve->setPen(pen);
    curve->setRenderHint(QwtPlotItem::RenderAntialiased);
    curve->setAxes(QwtPlot::xBottom, y_axis);
    curve->setStyle(QwtPlotCurve::Steps);

    return curve;
}

void Graph::updateCurve(QwtPlotCurve *curve, const QVector<QPointF> &data)
{
    QVector<QPointF> pr_data(data.size());
    coeffSolver toPercent = coeffSolver::genLevel2percentSolver();
    for (int i = 0; i < data.size(); ++i) {
        QPointF p = data.at(i);
        p.setY(toPercent.solve(p.y()));
        pr_data[i] = p;
    }
    curve->setSamples(pr_data);
}


void Graph::btn_range_selected(bool checked)
{
    if (!checked) {
        rangeSelector->setEnabled(true);
        return;
    }

    int r = -1;
    QObject *sigsender = sender();
    if (sigsender) {
        for(int i = 0; i < rangeselectors.size(); ++i) {
            if (sigsender == rangeselectors.at(i))
                r = i;
            else
                rangeselectors.at(i)->setChecked(false);
        }
    }
    else {
        for(int i = 0; i < rangeselectors.size(); ++i) {
            if (rangeselectors.at(i)->isChecked()) {
                r = i;
                break;
            }
        }
    }

    HistoryKeeper::dbinfo dbinfo;
    try {
        dbinfo = historyKeeper->dboverview();
    }
    catch(std::exception e) {
        return;
    }

    if (dbinfo.dbsize() == 0)
        return;

    rangeSelector->setEnabled(false);
    visableAreaEnd = dbinfo.LastDate();

    // {"Месяц", "Неделя", "День", "12 часов", "2 часа"};
    switch (r) {
    case 0: //
        visableAreaStart = visableAreaEnd.addMonths(-1);
        break;
    case 1:
        visableAreaStart = visableAreaEnd.addDays(-7);
        break;
    case 2:
        visableAreaStart = visableAreaEnd.addDays(-1);
        break;
    case 3:
        visableAreaStart = QDate_sub_time(visableAreaEnd, 12);
        break;
    case 4:
        visableAreaStart = QDate_sub_time(visableAreaEnd, 2);
        break;
    default:
        rangeSelector->setEnabled(true);
        return;
    }
    //if (visableAreaStart < dbinfo.StartDate())
    //    visableAreaStart = dbinfo.StartDate();

    // set scrollers
    rangeSelector->blockSignals(true);
    rangeSelector->setUpperPosition(SCROLL_SIZE);
    QDateTime amonthBack = dbinfo.LastDate().addMonths(-1);
    qint64 a = amonthBack.msecsTo(visableAreaStart);
    qint64 b = amonthBack.msecsTo(dbinfo.LastDate()) / SCROLL_SIZE;
    rangeSelector->setLowerPosition(a / b);
    rangeSelector->blockSignals(false);
}
