#include <math.h>

#include <QSet>

#include "presuresolver.h"

#define         T       T0 // нет температуры

PresureSolver::PresureSolver()
{
    reset();
}

float PresureSolver::solve(float input)
{
    return coeffs["A0"] +
            coeffs["A1"] * pow(T - coeffs["Ft0"], 1) +
            coeffs["A2"] * pow(T - coeffs["Ft0"], 2) +
            coeffs["A3"] * pow(input - coeffs["Fp0"], 1) +
            coeffs["A4"] * pow(input - coeffs["Fp0"], 2) +
            coeffs["A5"] * (T - coeffs["Ft0"]) * (input - coeffs["Fp0"]);
}

void PresureSolver::setT0(float T0)
{
    this->T0 = T0;
}

bool PresureSolver::setCoeffs(const QMap<QString, double> &coeffs)
{
    // check all coeffs present

    QSet<QString> keys_requred;
    keys_requred << "A0" << "A1" << "A2" << "A3" << "A4" << "A5" << "Ft0" << "Fp0";
    QList<QString> keys = coeffs.keys();
    foreach (QString key, keys_requred)
        if (!keys.contains(key))
            return false;

    this->coeffs = coeffs;
}

QMap<QString, double> PresureSolver::getCoeffs() const
{
    return coeffs;
}

void PresureSolver::reset()
{
    coeffs.insert("A0", 0);
    coeffs.insert("A1", 0);
    coeffs.insert("A2", 0);
    coeffs.insert("A3", 1);
    coeffs.insert("A4", 0);
    coeffs.insert("A5", 0);
    coeffs.insert("Ft0", 0);
    coeffs.insert("Fp0", 0);

    T0 = 0;
}

