#include <QString>

#include "percentscaledraw.h"

QwtText PercentScaleDraw::label(double v) const
{
    return QString::number(v) + " %";
}
