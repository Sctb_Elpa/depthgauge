#include <stdexcept>

#include <QDebug>
#include <QtSql>
#include <QApplication>
#include <QMessageBox>

#include <mylog/mylog.h>

#include <smoothspline/SmoothSpline.h>

#include "historykeeper.h"

#define OVERVIEW_MAX_SIZE       500

static const float testData[] = {15.7, 14.3, 12.8, 17.01, 8.15, 16.7, 5.3, 12.1};

static const QString tablename("history");

static const QString createtable_q("CREATE TABLE IF NOT EXISTS " + tablename + "("
                                   "id INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE,"
                                   "timestamp DATE,"
                                   "level FLOAT NOT NULL"
                                   ");"
                                   );

HistoryKeeper* HistoryKeeper::_this;

HistoryKeeper::HistoryKeeper(const QString &filename, QObject *parent) :
    QObject(parent)
{
    writeEnabled = true;
    db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(filename);
    if (!db.open())
    {
        throw std::runtime_error("Failed to open database");
    }

    // prepare DB if new
    QSqlQuery query;
    if (!query.exec(createtable_q))
        LOG_ERROR(trUtf8("Create table error: ").arg(query.lastError().databaseText()));
}

HistoryKeeper::~HistoryKeeper()
{
    db.close();
}

void HistoryKeeper::instantie(const QString &filename, QObject *parent)
{
    if (_this)
        return;
    _this = new HistoryKeeper(filename, parent);
}

HistoryKeeper *HistoryKeeper::getInstance()
{
    return _this;
}

HistoryKeeper::dbinfo HistoryKeeper::dboverview()
{
    // tableLen
    QSqlQuery query;
    int DBSize = dbLen();

    if (!query.exec(trUtf8("SELECT * FROM %1 ORDER BY timestamp ASC LIMIT 1;").arg(tablename)))
    {
        LOG_ERROR(trUtf8("Sql error get first row: %1").arg(query.lastError().databaseText()));
        throw std::length_error("failed get first row");
    }
    query.first();
    QDateTime first = query.value(1).toDateTime();
    if (!query.exec(trUtf8("SELECT * FROM %1 ORDER BY timestamp DESC LIMIT 1;").arg(tablename)))
    {
        LOG_ERROR(trUtf8("Sql error get last row: %1").arg(query.lastError().databaseText()));
        throw std::length_error("failed get last row");
    }
    query.first();
    QDateTime last = query.value(1).toDateTime();

    return dbinfo(first, last, DBSize);
}

int HistoryKeeper::dbLen()
{
    QSqlQuery query;
    if (!query.exec("SELECT COUNT(*) FROM " + tablename))
    {
        LOG_ERROR(trUtf8("Sql error get table size: %1").arg(query.lastError().databaseText()));
        throw std::length_error("failed to get size of data");
    }
    query.first();
    return query.value(0).toInt();
}

int HistoryKeeper::rangeLen(const QDateTime &from, const QDateTime &to) {
    QSqlQuery query;
    query.prepare(QString("SELECT COUNT (*) FROM %1 WHERE timestamp>='%2' and timestamp<='%3';").arg(
                      tablename).arg(from.toString(Qt::ISODate)).arg(to.toString(Qt::ISODate)));
    if (!query.exec() || !query.next()) {
        LOG_ERROR(trUtf8("Sql error get table size: %1").arg(query.lastError().databaseText()));
        throw std::length_error("failed to get size of data");
    }
    return query.value(0).toInt();
}

QVector<QPointF> HistoryKeeper::ReadRange(const QDateTime &from, const QDateTime &to, int maxpoints)
{
    QSqlQuery query;
    QString strfrom(from.toString(Qt::ISODate));
    QString strTo(to.toString(Qt::ISODate));

    int dataSize = rangeLen(from, to);
    if (maxpoints > 0 && dataSize > maxpoints) {
        int devider = dataSize / maxpoints + 1;
        query.prepare(trUtf8("SELECT * FROM %1 WHERE timestamp>='%2' and timestamp<='%3'"
                             "and %1.id % %4 == 0").arg(tablename).arg(strfrom).arg(strTo).arg(devider));
    } else {
        query.prepare(trUtf8("SELECT * FROM %1 WHERE timestamp>='%2' and timestamp<='%3';").arg(
                          tablename).arg(strfrom).arg(strTo));
    }
    if (!query.exec()) {
        LOG_ERROR(trUtf8("Sql error get data: %1").arg(query.lastError().databaseText()));
        throw std::length_error("failed to get size of data");
    }
    QVector<QPointF> res;
    while(query.next())
    {
        QPointF p(query.value(1).toDateTime().toMSecsSinceEpoch(), query.value(2).toFloat());
        res.append(p);
    }

    return res;
}

void HistoryKeeper::adddata(const QDateTime &timestamp, float value)
{
    if (writeEnabled)
    {
        QSqlQuery query;
        if (!query.exec(
                    trUtf8("INSERT INTO %1 (timestamp, level) VALUES ('%2', '%3')").arg(
                        tablename).arg(
                        timestamp.toString(Qt::ISODate)).arg(
                        value))
                )
            LOG_ERROR(trUtf8("insert error: %1").arg(query.lastError().databaseText()));

        writeEnabled = false;
    }
}

void HistoryKeeper::timerEvent(QTimerEvent *e)
{
    writeEnabled = true;
}

