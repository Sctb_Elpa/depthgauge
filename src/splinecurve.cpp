#include <qwt_painter.h>
#include <qwt_scale_map.h>
#include <qwt_clipper.h>

#include <QPointF>

#include "splinecurve.h"

#define POINTS_PRE_STEP     10

SplineCurve::SplineCurve(const QString &title, double smoothfactor) :
    QwtPlotCurve(title)
{
    spline_coeffs = NULL;
    setSmoothFactor(smoothfactor);
}

SplineCurve::SplineCurve(const QwtText &title, double smoothfactor) :
    QwtPlotCurve(title)
{
    spline_coeffs = NULL;
    setSmoothFactor(smoothfactor);
}

SplineCurve::~SplineCurve()
{
    if (spline_coeffs)
        delete spline_coeffs;
}

void SplineCurve::setSamples(const QVector<QPointF> & samples)
{
    QwtPlotCurve::setSamples(samples);

    if (spline_coeffs)
    {
        delete spline_coeffs;
        spline_coeffs = NULL;
    }

    if (samples.count() > 2)
    {

        int l = samples.count();

        double *x_ = new double[l];
        double *y_ = new double[l];

        for (int i = 0; i < l; ++i)
        {
            x_[i] = samples.at(i).x();
            y_[i] = samples.at(i).y();
        }

        SplineApprox(x_, y_, l, smoothfactor, &spline_coeffs);
    }
}

void SplineCurve::setSamples(const double *xData, const double *yData, int size)
{
    QVector<QPointF> samples;
    for (int i = 0; i < size; ++i)
        samples.append(QPointF(xData[i], yData[i]));
    setSamples(samples);
}

void SplineCurve::setSmoothFactor(double smoothfactor)
{
    this->smoothfactor = smoothfactor;
}

void SplineCurve::drawSteps(QPainter *painter, const QwtScaleMap &xMap, const QwtScaleMap &yMap, const QRectF &canvasRect, int from, int to) const
{
    //const bool doAlign = QwtPainter::roundingAlignment( painter );

    // полигон - это массив точек, тупо
    // в него нужног тупо напихать то, что мы хотим нарисовать

    if (spline_coeffs)
    {

        QPolygonF polygon( POINTS_PRE_STEP * ( to - from ) + 1);
        QPointF *points = polygon.data();

        const QwtSeriesData<QPointF> *series = data();

        double xStart = series->sample( from ).x();
        double xStop = series->sample( to ).x();

        double step = qAbs(xStop - xStart) / (polygon.size() - 1);

        for (int i = 0; i < polygon.size(); ++i)
        {
            double x = xStart + step * i;
            points[i].rx() = xMap.transform( x );
            points[i].ry() = yMap.transform( calculateSplineAt(spline_coeffs, series->size(), x).Y );
        }

        // эта фигня нарисует нам полигон
        QwtPainter::drawPolyline( painter, polygon );
        // а эта заполнит нужным цветом
        fillCurve( painter, xMap, yMap, canvasRect, polygon );
    }
    else
        QwtPlotCurve::drawLines(painter, xMap, yMap, canvasRect, from, to);
}

