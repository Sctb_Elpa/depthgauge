#include <QCalendarWidget>
#include <QLabel>
#include <QGridLayout>
#include <QPushButton>
#include <QDateTimeEdit>
#include <QMessageBox>
#include <QFile>
#include <QTextCodec>

#include <mylog/mylog.h>

#include "coeffsolver.h"
#include "mainform.h"
#include "historykeeper.h"
#include "reportgenerator.h"

ReportGenerator::ReportGenerator(const QDateTime& rangeStart, const QDateTime& rangeEnd, QWidget *parent) :
    RecordSetup(parent, true)
{
    from_t = new QDateTimeEdit;
    from_t->setDisplayFormat("dd.MM.yyyy hh:mm:ss");
    from_t->setCalendarPopup(true);
    from_t->setDateTime(rangeStart);

    to_t = new QDateTimeEdit;
    to_t->setDisplayFormat("dd.MM.yyyy hh:mm:ss");
    to_t->setCalendarPopup(true);
    to_t->setDateTime(rangeEnd);

    QGridLayout *layout = new QGridLayout;
    layout->addWidget(new QLabel(trUtf8("C:")), 0, 0);
    layout->addWidget(from_t, 0, 1);
    layout->addWidget(new QLabel(trUtf8("по:")), 1, 0);
    layout->addWidget(to_t, 1, 1);

    getMainlayout()->insertLayout(2, layout);

    connect(this, SIGNAL(accepted()), this, SLOT(createReport()));
}

void ReportGenerator::createReport(const QString &filename, int devider)
{
    LOG_INFO(trUtf8("Creating Report [%1 --> %2]").arg(
                 from_t->dateTime().toString(DATE_WRITE_FORMAT)).arg(
                 to_t->dateTime().toString(DATE_WRITE_FORMAT)));

    QVector<QPointF> res;
    try
    {
        res = HistoryKeeper::getInstance()->ReadRange(from_t->dateTime(), to_t->dateTime());
    }
    catch(std::exception e)
    {
        return;
    }

    if (res.size() > 0)
    {
        coeffSolver solver = coeffSolver::genLevel2percentSolver();
        QVariantMap settings = getSetup();
        QFile f(filename.isEmpty() ? settings.value("filename").toString() : filename);

        if (!f.open(QIODevice::WriteOnly | QIODevice::Text))
        {
            QMessageBox::warning(this, trUtf8("Ошибка записи"),
                                 trUtf8("Не удалось создать файл %1, попробуйте еще раз").arg(f.fileName()));
            LOG_ERROR(trUtf8("Failed to open %1").arg(f.fileName()));
            return;
        }
        QByteArray outdata;
        QTextCodec *codec = QTextCodec::codecForName("Windows-1251");
        outdata.append(codec->fromUnicode(trUtf8("Дата;Уровень [м];Уровень [%]\n")));

        f.write(outdata);

        if (devider <= 0)
            devider = settings["WI"].toInt();

        qint64 i = 0;
        foreach (QPointF p, res)
            if (i++ % devider == 0)
                f.write(trUtf8("%1;%2;%3\n").arg(
                            QDateTime::fromMSecsSinceEpoch(p.x()).toString(DATE_WRITE_FORMAT)).arg(
                            p.y(), 0, 'f', 2).arg(solver.solve(p.y()), 0, 'f', 1).toLatin1());

        f.close();
    }
    else
    {
        if (!filename.isEmpty()) // trap
            return;
        QMessageBox::warning(this, trUtf8("Нечего выгружать"),
                             trUtf8("Выбранный диопазон не содержит измерений, выберите правильный диопазон"));
        LOG_ERROR(trUtf8("No data in selected range"));
    }
}


