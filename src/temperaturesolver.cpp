#include <math.h>

#include <QSet>

#include "temperaturesolver.h"

temperatureSolver::temperatureSolver() : solver()
{
    reset();
}

float temperatureSolver::solve(float input)
{
    return coeffs["P0"] +
            coeffs["C1"] * pow(input - coeffs["F0"], 1) +
            coeffs["C2"] * pow(input - coeffs["F0"], 2) +
            coeffs["C3"] * pow(input - coeffs["F0"], 3);
}

void temperatureSolver::reset()
{
    coeffs.insert("C1", 1);
    coeffs.insert("C2", 0);
    coeffs.insert("C3", 0);
    coeffs.insert("P0", 0);
    coeffs.insert("F0", 0);
}

bool temperatureSolver::setCoeffs(const QMap<QString, double> &coeffs)
{
    QSet<QString> keys_requred;
    keys_requred << "C1" << "C2" << "C3" << "P0" << "F0";
    QList<QString> keys = coeffs.keys();
    foreach (QString key, keys_requred)
        if (!keys.contains(key))
            return false;

    this->coeffs = coeffs;
}

QMap<QString, double> temperatureSolver::getCoeffs() const
{
    return coeffs;
}
