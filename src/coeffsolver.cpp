#include <QString>

#include <settingscmdline/settingscmdline.h>

#include "coeffsolver.h"

coeffSolver::coeffSolver(double coeff)
{
    this->coeff = coeff;
}

float coeffSolver::solve(float input)
{
    return input * coeff;
}

void coeffSolver::reset()
{
    coeff = 0;
}

QMap<QString, double> coeffSolver::getCoeffs() const
{
    QMap<QString, double> res;
    res.insert("C", coeff);
    return res;
}

bool coeffSolver::setCoeffs(const QMap<QString, double> &coeffs)
{
    if (coeffs.keys().contains("C"))
    {
        coeff = coeffs.value("C");
        return true;
    }
    else
        return false;
}

coeffSolver coeffSolver::genLevel2percentSolver()
{
    return coeffSolver(SettingsCmdLine::settings->value("Graph/Percent").toDouble() /
                       SettingsCmdLine::settings->value("Graph/Level").toDouble());
}
