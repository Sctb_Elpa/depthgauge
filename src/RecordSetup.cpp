
#include <QPushButton>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QGroupBox>
#include <QRadioButton>
#include <QCheckBox>
#include <QLineEdit>
#include <QFileDialog>
#include <QDir>
#include <QDateTime>
#include <QSpinBox>
#include <QLabel>
#include <QMessageBox>

#include "settingscmdline/settingscmdline.h"

#include "mainform.h"
#include "RecordSetup.h"

RecordSetup::RecordSetup(QWidget *parent, bool tiny) :
    QDialog(parent)
{
    this->tiny = tiny;
    result = false;
    OkButton = new QPushButton(trUtf8("Ok"));
    CancelButton = new QPushButton(trUtf8("Отмена"));

    mainlayout = new QVBoxLayout;

    if (!tiny)
    {
        QGroupBox *radiogroup = new QGroupBox(trUtf8("Колонка даты"));

        radiogroup_m[0] = new QRadioButton(trUtf8("Системное время [%1]").arg(QDateTime::currentDateTime().toString(DATE_WRITE_FORMAT)));
        radiogroup_m[1] = new QRadioButton(trUtf8("Таймер [01 02:03:04]"));
        radiogroup_m[2] = new QRadioButton(trUtf8("Нумерация (1, 2, 3...)"));
        radiogroup_m[3] = new QRadioButton(trUtf8("Нет"));
        QVBoxLayout *radiolayout = new QVBoxLayout;
        for (int i = 0; i < 4; ++i)
        {
            radiolayout->addWidget(radiogroup_m[i]);
        }

        int selectedMode = SettingsCmdLine::settings->value("SaveSettings/DateMode", 0).toInt();
        if (selectedMode >= 0 && selectedMode < 4)
            radiogroup_m[selectedMode]->setChecked(true);
        else
            radiogroup_m[0]->setChecked(true);

        radiogroup->setLayout(radiolayout);

        mainlayout->addWidget(radiogroup);

        WriteLevel = new QCheckBox(trUtf8("Записывать уровень"));
        WriteLevel_pr = new QCheckBox(trUtf8("Записывать уровень в процентах"));
        WriteAtm_p = new QCheckBox(trUtf8("Записывать атмосферное давление"));
        WriteReservuar_p = new QCheckBox(trUtf8("Записывать давление в резервуаре"));

        WriteLevel->setChecked(SettingsCmdLine::settings->value("SaveSettings/WL", true).toBool());
        WriteLevel_pr->setChecked(SettingsCmdLine::settings->value("SaveSettings/WLP", true).toBool());
        WriteAtm_p->setChecked(SettingsCmdLine::settings->value("SaveSettings/WA", true).toBool());
        WriteReservuar_p->setChecked(SettingsCmdLine::settings->value("SaveSettings/WR", true).toBool());

        mainlayout->addWidget(WriteLevel);
        mainlayout->addWidget(WriteLevel_pr);
        mainlayout->addWidget(WriteAtm_p);
        mainlayout->addWidget(WriteReservuar_p);
    }



    QHBoxLayout *writeintervallayout = new QHBoxLayout;
    writeintervallayout->addWidget(new QLabel(trUtf8("Интервал записи [c]")));
    writeInterval = new QSpinBox;
    writeInterval->setMinimum(1);
    writeInterval->setMaximum(12*60*60); // 12h
    writeInterval->setValue(SettingsCmdLine::settings->value("SaveSettings/WI", 1).toInt());
    writeintervallayout->addWidget(writeInterval);

    mainlayout->addLayout(writeintervallayout);

    QHBoxLayout *filenamelayout = new QHBoxLayout;
    FilenameEdit = new QLineEdit(
                SettingsCmdLine::settings->value("SaveSettings/filename", "output.csv").toString());
    SetFilenameBtn = new QPushButton("...");
    filenamelayout->addWidget(FilenameEdit);
    filenamelayout->addWidget(SetFilenameBtn);
    mainlayout->addLayout(filenamelayout);

    QHBoxLayout *buttonsLayut = new QHBoxLayout;
    buttonsLayut->addWidget(OkButton);
    buttonsLayut->addWidget(CancelButton);
    mainlayout->addLayout(buttonsLayut);

    connect(OkButton, SIGNAL(clicked()), this, SLOT(okSlot()));
    connect(CancelButton, SIGNAL(clicked()), this, SLOT(reject()));
    connect(SetFilenameBtn, SIGNAL(clicked()), this, SLOT(SelectFilenameSlot()));

    setLayout(mainlayout);
}

QVariantMap RecordSetup::getSetup()
{
    if (result)
    {
        QVariantMap result;
        if (!tiny)
        {
            for (int i = 0; i < 4; ++i)
            {
                if (radiogroup_m[i]->isChecked())
                {
                    result["dateMode"] = i;
                    break;
                }
            }
            result["WL"] = WriteLevel->isChecked();
            result["WLP"]= WriteLevel_pr->isChecked();
            result["WA"] = WriteAtm_p->isChecked();
            result["WR"] = WriteReservuar_p->isChecked();
        }
        result["WI"] = writeInterval->value();
        result["filename"] = FilenameEdit->text();
        return result;
    }
    else
        return QVariantMap();
}

QVBoxLayout *RecordSetup::getMainlayout() const
{
    return mainlayout;
}

void RecordSetup::okSlot()
{
    QFile f(FilenameEdit->text());
    if (f.exists())
    {
        int r = QMessageBox::question(this, trUtf8("Подтверждение презаписи"),
                                      trUtf8("Файл %1 существует, заменить?").arg(f.fileName()),
                                      trUtf8("Да"), trUtf8("Нет"), QString(), 1, 0);
        if (r)
            return;
    }

    result = true;
    accept();
    if (!tiny)
        close();
}

void RecordSetup::SelectFilenameSlot()
{
    QString res = QFileDialog::getSaveFileName(this,
                                               trUtf8("Сохранить как.."), QDir::currentPath(),
                                               trUtf8("csv (*.csv)"));
    if (!res.isEmpty())
    {
        if (!res.endsWith(".csv", Qt::CaseInsensitive))
            res += ".csv";

        FilenameEdit->setText(res);
    }
}
