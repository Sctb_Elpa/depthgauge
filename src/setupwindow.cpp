#include <QDoubleSpinBox>
#include <QLineEdit>
#include <QSlider>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QGroupBox>
#include <QLabel>
#include <QGridLayout>
#include <QLineEdit>
#include <QPushButton>
#include <QCheckBox>
#include <QFileDialog>

#include <settingscmdline/settingscmdline.h>

#include "setupwindow.h"

#define MAX_LEVEL_DEVIATION     (5)
#define SLIDER_RESOLUTION       (100)

SetupWindow::SetupWindow(QWidget *parent) :
    QDialog(parent)
{
    QVBoxLayout *rootlayout = new QVBoxLayout;

    ZeroCorrectionSlider = new QSlider(Qt::Horizontal);
    ZeroCorrectionSlider->setMinimum(-MAX_LEVEL_DEVIATION * 100);
    ZeroCorrectionSlider->setMaximum(MAX_LEVEL_DEVIATION * 100);

    ZeroCorrectionSpinbox = new QDoubleSpinBox;
    ZeroCorrectionSpinbox->setMinimum(-MAX_LEVEL_DEVIATION);
    ZeroCorrectionSpinbox->setMaximum(MAX_LEVEL_DEVIATION);
    ZeroCorrectionSpinbox->setSingleStep(0.01);

    QGroupBox *ZeroCorrectionBox = new QGroupBox(trUtf8("Подстройка нуля [м]"));

    QHBoxLayout* ZeroCorrectionLayout = new QHBoxLayout;
    ZeroCorrectionLayout->addWidget(ZeroCorrectionSlider);
    ZeroCorrectionLayout->addWidget(ZeroCorrectionSpinbox);
    ZeroCorrectionBox->setLayout(ZeroCorrectionLayout);

    rootlayout->addWidget(ZeroCorrectionBox);

    QLabel *percentageLabel = new QLabel(trUtf8("Процент заполненности [%]"));
    PercentSpinbox = new QDoubleSpinBox();
    PercentSpinbox->setSingleStep(0.01);
    PercentSpinbox->setRange(1, 100);
    QLabel *LevelLabel = new QLabel(trUtf8("Высота уровня [м]"));
    LevelSpinbox = new QDoubleSpinBox();
    LevelSpinbox->setSingleStep(0.01);
    LevelSpinbox->setRange(0.1, 100);
    QLabel *resultLabel = new QLabel(trUtf8("Коэфициент пересчета"));
    resultcoeff = new QLineEdit();
    resultcoeff->setEnabled(false);

    QGridLayout* grid = new QGridLayout;
    grid->addWidget(percentageLabel, 0, 0);
    grid->addWidget(PercentSpinbox, 0, 1);
    grid->addWidget(LevelLabel, 1, 0);
    grid->addWidget(LevelSpinbox, 1, 1);
    grid->addWidget(resultLabel, 2, 0);
    grid->addWidget(resultcoeff, 2, 1);

    QGroupBox *CoeffBox = new QGroupBox(trUtf8("Подстройка нуля"));
    CoeffBox->setLayout(grid);

    rootlayout->addWidget(CoeffBox);

    QGroupBox *Autoreports = new QGroupBox(trUtf8("Автоматические отчеты"));
    Reportsenabled = new QCheckBox(trUtf8("Разрешить"));
    QLabel* AutoreportsPeriod_desc = new QLabel(trUtf8("Интервал генерации отчета [c]"));
    QLabel* AutoreportsDataCount_desc = new QLabel(trUtf8("Количество точек"));

    QLabel *AutoreportDir = new QLabel(trUtf8("Каталог для сохранения"));
    SaveDir = new QLineEdit;
    QPushButton* brouseButton = new QPushButton(trUtf8("Обзор"));
    QHBoxLayout* Catalogselectlayout = new QHBoxLayout();
    Catalogselectlayout->addWidget(SaveDir);
    Catalogselectlayout->addWidget(brouseButton);

    AutoreportsPeriod = new QSpinBox();
    AutoreportsPeriod->setMinimum(1);
    AutoreportsPeriod->setMaximum(2 * 30 * 24 * 60 * 60); // 2m

    AutoreportsDataCount = new QSpinBox();
    AutoreportsDataCount->setMinimum(2);

    QGridLayout* autoreportGrid = new QGridLayout();
    autoreportGrid->addWidget(Reportsenabled, 0, 0);
    autoreportGrid->addWidget(AutoreportsPeriod_desc, 1, 0);
    autoreportGrid->addWidget(AutoreportsPeriod, 1, 1);
    autoreportGrid->addWidget(AutoreportsDataCount_desc, 2, 0);
    autoreportGrid->addWidget(AutoreportsDataCount, 2, 1);
    autoreportGrid->addWidget(AutoreportDir, 3, 0);
    autoreportGrid->addLayout(Catalogselectlayout, 3, 1);

    Autoreports->setLayout(autoreportGrid);

    rootlayout->addWidget(Autoreports);

    QPushButton* OkButton = new QPushButton(trUtf8("OK"));
    QPushButton* cancelButton = new QPushButton(trUtf8("Отмена"));

    QHBoxLayout *btnlayout = new QHBoxLayout;
    btnlayout->addWidget(OkButton);
    btnlayout->addWidget(cancelButton);

    rootlayout->addLayout(btnlayout);

    setLayout(rootlayout);

    //

    connect(ZeroCorrectionSlider, SIGNAL(valueChanged(int)), this, SLOT(sliderValueChanged(int)));
    connect(ZeroCorrectionSpinbox, SIGNAL(valueChanged(double)), this, SLOT(ZeroCorrectionSpinboxChanged(double)));

    connect(PercentSpinbox, SIGNAL(valueChanged(double)), this, SLOT(coeff_changed()));
    connect(LevelSpinbox, SIGNAL(valueChanged(double)), this, SLOT(coeff_changed()));

    connect(OkButton, SIGNAL(clicked()), this, SLOT(accept()));
    connect(cancelButton, SIGNAL(clicked()), this, SLOT(reject()));

    connect(Reportsenabled, SIGNAL(clicked(bool)), AutoreportsPeriod_desc, SLOT(setEnabled(bool)));
    connect(Reportsenabled, SIGNAL(clicked(bool)), AutoreportsDataCount_desc, SLOT(setEnabled(bool)));
    connect(Reportsenabled, SIGNAL(clicked(bool)), AutoreportsPeriod, SLOT(setEnabled(bool)));
    connect(Reportsenabled, SIGNAL(clicked(bool)), AutoreportsDataCount, SLOT(setEnabled(bool)));
    connect(Reportsenabled, SIGNAL(clicked(bool)), AutoreportDir, SLOT(setEnabled(bool)));
    connect(Reportsenabled, SIGNAL(clicked(bool)), SaveDir, SLOT(setEnabled(bool)));
    connect(Reportsenabled, SIGNAL(clicked(bool)), brouseButton, SLOT(setEnabled(bool)));

    connect(AutoreportsPeriod, SIGNAL(valueChanged(int)), this, SLOT(updatePointsMax(int)));
    connect(brouseButton, SIGNAL(clicked()), this, SLOT(BrouseCatalogs()));

    //

    originalZero = SettingsCmdLine::settings->value(
                "Global/ZeroOffset").toDouble();
    originalP = SettingsCmdLine::settings->value(
                "Graph/Percent").toDouble();
    originalLevel = SettingsCmdLine::settings->value(
                "Graph/Level").toDouble();

    Reportsenabled->setChecked(SettingsCmdLine::settings->value("AutoReport/Enabled").toBool());
    AutoreportsPeriod->setValue(SettingsCmdLine::settings->value("AutoReport/Period").toInt());
    AutoreportsDataCount->setValue(SettingsCmdLine::settings->value("AutoReport/Points").toInt());
    SaveDir->setText(SettingsCmdLine::settings->value("AutoReport/Directory").toString());

    ZeroCorrectionSpinbox->setValue(originalZero);
    PercentSpinbox->setValue(originalP);
    LevelSpinbox->setValue(originalLevel);
}

void SetupWindow::apply_settings()
{
    (*SettingsCmdLine::settings)["Global/ZeroOffset"] = ZeroCorrectionSpinbox->value();
    (*SettingsCmdLine::settings)["Graph/Percent"] = PercentSpinbox->value();
    (*SettingsCmdLine::settings)["Graph/Level"] = LevelSpinbox->value();
}

void SetupWindow::sliderValueChanged(int newVal)
{
    ZeroCorrectionSpinbox->blockSignals(true);
    ZeroCorrectionSpinbox->setValue((double)newVal / SLIDER_RESOLUTION);
    ZeroCorrectionSpinbox->blockSignals(false);

    apply_settings();
}

void SetupWindow::ZeroCorrectionSpinboxChanged(double newVal)
{
    ZeroCorrectionSlider->blockSignals(true);
    ZeroCorrectionSlider->setValue(qRound(newVal * SLIDER_RESOLUTION));
    ZeroCorrectionSlider->blockSignals(false);

    apply_settings();
}

void SetupWindow::coeff_changed()
{
    resultcoeff->setText(QString::number(PercentSpinbox->value() / LevelSpinbox->value()));

    apply_settings();
}

void SetupWindow::updatePointsMax(int period)
{
    AutoreportsDataCount->setMaximum(period);
}

void SetupWindow::BrouseCatalogs()
{
    QString res = QFileDialog::getExistingDirectory(
                this, trUtf8("Выберите каталог для сохранения"),
                SaveDir->text());
    if (!res.isEmpty())
    {
        SaveDir->setText(res);
    }
}

void SetupWindow::accept()
{
    (*SettingsCmdLine::settings)["AutoReport/Enabled"] = Reportsenabled->isChecked();
    (*SettingsCmdLine::settings)["AutoReport/Period"] = AutoreportsPeriod->value();
    (*SettingsCmdLine::settings)["AutoReport/Points"] = AutoreportsDataCount->value();
    (*SettingsCmdLine::settings)["AutoReport/Directory"] = SaveDir->text();

    QDialog::accept();
}

void SetupWindow::reject()
{
    (*SettingsCmdLine::settings)["Global/Zero_offset"] = originalZero;
    (*SettingsCmdLine::settings)["Graph/Percent"] = originalP;
    (*SettingsCmdLine::settings)["Graph/Level"] = originalLevel;

    QDialog::reject();
}
